import sqlite3
class Gestion_Estudiantes:
    def __init__(self):
        self.conectar = sqlite3.connect('academia.db')
        self.cursor = self.conectar.cursor()
        self.cursor.execute("""CREATE TABLE IF NOT EXISTS usuarios( idusuario INT PRIMARY KEY, nombre TEXT, email TEXT);
        """)
        self.conectar.commit()
        print("Establecer una Conexión")

    def Matricular_Estudiantes(self):
        print("REGISTRO DE ESTUDIANTES:")
        id = str(input("ID ESTUDIANTE:\t"))
        nombre = str(input("NOMBRES:\t"))
        email = str(input("EMAIL:\t"))
        informacion = """INSERT INTO usuarios(idusuario, nombre, email) VALUES (?, ?,?) """
        datos = (id, nombre, email)
        self.cursor.execute(informacion, datos)
        self.conectar.commit()
        print("DATOS GUARDADOS")
        Volver = input("\nRegistar mas estudiantes:\t")
        if Volver == 'Y' or Volver == 'y':
            self.Matricular_Estudiantes()
        elif Volver == 'N' or Volver == 'n':
            self.menu()

    def Visualizar_Datos_Estudiantes(self):
        print("Estudiantes Registrados")
        var = self.cursor.execute("""SELECT *  FROM usuarios""")
        for i in var:
            print ("ID =", i[0])
            print("NOMBRES =", i[1])
            print("EMAIL =", i[2])
            print("")

        print("Datos ingresados")
        Volver = input("\nVolver al menu:\t")
        if Volver == 'inicio' or Volver == 'inicio':
            self.menu()
        elif Volver == 'inicio' or Volver == 'inicio':
            self.conectar.close()
            print('CONEXION FINALIZADA')

    def Buscar_Estudiante_ID(self):
        id = str(input("Ingresar ID del usuario:\t"))
        self.cursor.execute("SELECT * FROM usuarios WHERE idusuario = (?)", (id,))
        var = self.cursor.fetchall()
        for i in var:
            print("ID = ", i[0])
            print("NOMBRE = ", i[1])
            print("EMAIL = ", i[2])
            Volver = input("\nBuscar otro estudiante:\t")
            if Volver == 'inicio' or Volver == 'inicio':
                self.Buscar_Estudiante_ID()
            elif Volver == 'inicio' or Volver == 'inicio':
                self.menu()


    def Eliminar_Estudiante_ID(self):
        print("Eliminando Alumno")
        id = str(input("ID de estudiante eliminado:\t"))
        self.cursor.execute("DELETE FROM usuarios WHERE idusuario = (?)", (id,))
        self.conectar.commit()
        print("ESTUDIANTE ELIMINADO")
        Volver = input("\nEliminar otro Estudiante:\t")
        if Volver == 'inicio' or Volver == 'inicio':
            self.Eliminar_Estudiante_ID()
        elif Volver == 'inicio' or Volver == 'inicio':
            self.menu()

    def menu(self):
        print("Gestion estudiantes")
        print("""
            a) Matricular Estudiantes                 
            b) Visualizar Datos del Estudiantes
            c) Buscar Estudiante por ID
            d) Eliminar Estuiante por ID
            x) Salir \n """)

        selec = input("Ingrese una opcion:\t")

        if selec == "a":
            self.Matricular_Estudiantes()
        elif selec == "b":
            self.Visualizar_Datos_Estudiantes()
        elif selec == "c":
            self.Buscar_Estudiante_ID()
        elif selec == "d":
            self.Eliminar_Estudiante_ID()
        elif selec == "x":
            print("Terminar la Conexion")
            self.conectar.close()

        else:
            print("Terminar la Conexion")
            self.conectar.close()
            print("Terminar la Conexion")

if __name__ == "__main__":
    inicio =Gestion_Estudiantes()
    inicio.menu()